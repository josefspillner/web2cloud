import bs4
import pyesprima
import json
import os
import sys

debug = True

def process(ast, functionname):
	astjs = json.dumps(str(ast))
	if debug:
		print("pythonic astjs: " + astjs)

	substitutions = {"u'": "'", "True": "true", "False": "false", "None": "null", "'": "\""}
	for search, replace in substitutions.items():
		astjs = astjs.replace(search, replace)
	astjs = astjs[1:-1]
	if debug:
		print("pure astjs: " + astjs)

	fjs = open("_encoder." + functionname + ".js", "w")
	fjs.write("escodegen = require(\"escodegen\");\n")
	fjs.write("code = escodegen.generate(" + astjs + ");\n")
	fjs.write("process.stdout.write(code);")
	fjs.close()

	if debug:
		tjs = open("_ast." + functionname + ".js", "w")
		tjs.write(json.dumps(json.loads(astjs), indent=2))
		#tjs.write(astjs)
		tjs.close()

	os.system("node _encoder." + functionname + ".js > _lambda." + functionname + ".js")
	if not debug:
		os.unlink("_encoder." + functionname + ".js")

	ljs = open("_lambda." + functionname + ".js", "a")
	ljs.write("\n")
	ljs.write("exports.myHandler = function(event, context){\n")
	ljs.write("\t" + functionname + "();\n")
	ljs.write("}\n")
	ljs.close()

htmlfile = "index.html"
if len(sys.argv) == 2:
	htmlfile = sys.argv[1]

content = open(htmlfile).read()

soup = bs4.BeautifulSoup(content, "lxml")
scripts = soup.find_all('script')
for script in scripts:
	if debug:
		print("extracted script: " + str(script))

	ast = pyesprima.parse(script.get_text())
	if debug:
		print("python ast: " + str(ast))

	for body in ast.body:
		if body["type"] == "FunctionDeclaration":
			if debug:
				print("ast function detected: " + body["id"]["name"])
				process(body, body["id"]["name"])
