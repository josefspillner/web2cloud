# -*- coding: utf-8 -*-

from miproxy.proxy import AsyncMitmProxy, ProxyHandler
import mimetools
import StringIO
import zlib
import gzip

class MitmProxyHandler(ProxyHandler):
	def mitm_request(self, data):
		print '>> %s' % repr(data[:100])
		return data

	def mitm_response(self, data):
		headers, body = data.split("\r\n\r\n", 1)
		#print "headers-first", headers
		datalines = headers.split("\r\n", 1)
		request_line = datalines[0]
		headers_lines = datalines[1]
		mimeheaders = mimetools.Message(StringIO.StringIO(headers_lines))
		print "headers»", len(mimeheaders), mimeheaders.keys()
		#print "request line»", request_line
		#print "headers alone»", headers_lines
		#print "body»", body

		#Content-Encoding: gzip
		#Content-Type: text/html
		if "Content-Type" in mimeheaders and mimeheaders["Content-Type"] == "text/html":
			compressed = False
			if "Content-Encoding" in mimeheaders and mimeheaders["Content-Encoding"] == "gzip":
				compressed = True
				body = zlib.decompress(body, 16 + zlib.MAX_WBITS)
			print "types»", type(headers), type(body), type(data)
			print "content»", headers, body
			body = body.replace("o", "ö")
			if compressed:
				#body = zlib.compress(body)
				zbuf = StringIO.StringIO()
				zfile = gzip.GzipFile(None, "wb", 9, zbuf)
				zfile.write(body)
				zfile.close()
				body = zbuf.getvalue()
				clen = len(body)
				# FIXME: use regexp
				headers = headers.replace("Content-Length: 1835", "Content-Length: %i" % clen)
				print "recompressed»", headers, body

			data = headers + "\r\n\r\n" + body

		print '<< %s' % repr(data[:100])
		return data

if __name__ == '__main__':
	proxy = AsyncMitmProxy(RequestHandlerClass=MitmProxyHandler)
	try:
		proxy.serve_forever()
	except KeyboardInterrupt:
		proxy.server_close()
